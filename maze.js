const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
]; //21x15

function displayBlock(blockChar, rowDiv) {
    console.log(blockChar)
    let blockDiv = document.createElement('div')
    blockDiv.classList.add("block")
    if (blockChar === "W") {
        blockDiv.classList.add("wall")
    } else if (blockChar === " ") {
        blockDiv.classList.add("space")
    } else if (blockChar === "S") {
        blockDiv.classList.add('start')
    } else if (blockChar === "F") {
        blockDiv.classList.add("finish")
    }
    rowDiv.appendChild(blockDiv)
}

function displayRow(rowStr, index) {
    const maze = document.querySelector("#maze")
    let rowDiv = document.createElement('div')
    rowDiv.classList.add("row")
    maze.appendChild(rowDiv)

    for (let colNum = 0; colNum < rowStr.length; colNum++) {
        displayBlock(rowStr.charAt(colNum), rowDiv)

    }
}

map.forEach(displayRow)

//add event listener for keyDown press
document.addEventListener('keydown', logKey)
//adding function for they eventlistener
function logKey(e) {
    console.log(e.code)
    if (e.code === 'ArrowRight') {
        if (map[currentRow][currentColumn + 1] !== "W") {
            startLeft += 20
            currentColumn += 1
            document.querySelector(".start").style.left = startLeft + "px";
        }
    } else if (e.code === 'ArrowLeft') {
        if (map[currentRow][currentColumn - 1] !== "W") {
            startLeft -= 20
            currentColumn -= 1
            document.querySelector(".start").style.left = startLeft + "px";
        }
    } else if (e.code === 'ArrowDown') {
        if (map[currentRow + 1][currentColumn] !== "W") {
            startTop += 20
            currentRow += 1
            document.querySelector(".start").style.top = startTop + "px";
        }
    } else if (e.code === 'ArrowUp') {
        if (map[currentRow - 1][currentColumn] !== "W") {
            startTop -= 20
            currentRow -= 1
            document.querySelector(".start").style.top = startTop + "px";
        }
    }
    if (map[currentRow][currentColumn] === map[winningRow][winningColumn]) {
        alert("YOU WON")
    }  
}

let startTop = 0
let startLeft = 0

let currentRow = 9
let currentColumn = 0

let winningRow = 8
let winningColumn = 20